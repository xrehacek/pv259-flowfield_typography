/*
 * From the book "The nature of code" by Daniel Shiffman
 * chapter 6.4: Steering Behaviors: Flow Field Following
 */

class Agent {
  PVector location;
  PVector velocity;
  PVector acceleration;

  float point_size;
  float maxforce;
  float maxspeed;
  
  color color_fill;
 
  Agent(float x, float y, color fill, float size, float max_force, float max_speed) {
    color_fill = fill;
    point_size = size;
    acceleration = new PVector(0,0);
    velocity = new PVector(0,0);
    location = new PVector(x,y);
    maxspeed = max_speed;
    maxforce = max_force;
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(maxspeed);
    location.add(velocity);
    acceleration.mult(0);
  }

  void applyForce(PVector force) {
    acceleration.add(force);
  }

  private void seek(PVector target) {
    PVector desired = PVector.sub(target,location);
    desired.normalize();
    desired.mult(maxspeed);
    PVector steer = PVector.sub(desired,velocity);
    steer.limit(maxforce);
    applyForce(steer);
  }
  
  void follow(FlowField flow) {
    PVector desired = flow.lookup(location);
    desired.mult(maxspeed);

    PVector steer = PVector.sub(desired, velocity);
    steer.limit(maxforce);
    applyForce(steer);
  }
  
  void draw(PGraphics f) {
    f.noStroke();
    f.fill(color_fill);
    f.pushMatrix();
    f.translate(location.x,location.y);
    f.ellipse(0, 0, point_size,point_size);
    f.popMatrix();
  }
}
