/*
 * https://generateme.wordpress.com/
 */
static class Noise {
  static PVector circle(float n) { // polar to cartesian coordinates
    return new PVector(cos(n), sin(n));
  }
   
  static PVector astroid(float n) {
    float sinn = sin(n);
    float cosn = cos(n);
   
    float xt = sq(sinn)*sinn;
    float yt = sq(cosn)*cosn;
   
    return new PVector(xt, yt);
  }
   
  static PVector cissoid(float n) {
    float sinn2 = 2*sq(sin(n));
   
    float xt = sinn2;
    float yt = sinn2*tan(n);
   
    return new PVector(xt, yt);
  }
   
  static PVector kampyle(float n) {
    float sec = 1/sin(n);
   
    float xt = sec;
    float yt = tan(n)*sec;
   
    return new PVector(xt, yt);
  }
   
  static PVector rect_hyperbola(float n) {
    float sec = 1/sin(n);
   
    float xt = 1/sin(n);
    float yt = tan(n);
   
    return new PVector(xt, yt);
  }
   
  final static float superformula_a = 1;
  final static float superformula_b = 1;
  final static float superformula_m = 6;
  final static float superformula_n1 = 1;
  final static float superformula_n2 = 7;
  final static float superformula_n3 = 8;
  static PVector superformula(float n) {
    float f1 = pow(abs(cos(superformula_m*n/4)/superformula_a), superformula_n2);
    float f2 = pow(abs(sin(superformula_m*n/4)/superformula_b), superformula_n3);
    float fr = pow(f1+f2, -1/superformula_n1);
   
    float xt = cos(n)*fr;
    float yt = sin(n)*fr;
   
    return new PVector(xt, yt);
  }
  
  // helpers
  static PVector vexp(PVector p, float weight) {
    float r = weight * exp(p.x);
    return new PVector(r * cos(p.y), r * sin(p.y));
  }
  
  static PVector swirl(PVector p, float weight) {
    float r2 = sq(p.x)+sq(p.y);
    float sinr = sin(r2);
    float cosr = cos(r2);
    float newX = 0.8 * (sinr * p.x - cosr * p.y);
    float newY = 0.8 * (cosr * p.y + sinr * p.y);
    return new PVector(weight * newX, weight * newY);
  }
  
  static PVector waves2(PVector p, float weight) {
    float x = weight * (p.x + 0.9 * sin(p.y * 4));
    float y = weight * (p.y + 0.5 * sin(p.x * 5.555));
    return new PVector(x, y);
  }
}
