/**
 * Generative typography using flowfields
 *
 * Ffmpeg is required for video export.
 *
 * Some hints for usage:
 * - ESC to exit
 * - pressing TAB hides/shows controls
 * - hold "`" or ";" key to record. When you release the key, high resolution render 
 *   without UI will be saved in current folder + parameters used to produce it
 * - you can load parameters savefile by dropping it in the window (use alt+tab 
 *   in fullscreen to show file manager). background color won't be loaded
 *
 * @author Marko Rehacek <456490@mail.muni.cz>
 */

import java.util.*;
import geomerative.*; // easily work with fonts and shapes and stuff
import controlP5.*;   // GUI
import drop.*;        // detect dropping files into window
import com.hamoid.*;  // video export

VideoExport videoExport;
PGraphics pg;

// rendering size for export
// A4 @ 300dpi is 3508x2480
int 
REC_BUFFER_WIDTH = 3840
, REC_BUFFER_HEIGHT = 2160;

Main app;
ControlP5 cp5;
SDrop drop;
PGraphics main_layer;
PGraphics rec_layer; // frame buffer for export
boolean recording = false, rec_initializing = false;
String record_time;

String load_params = ""; // if path is stored here, cp5 will try to load UI parameters in next update cycle
String load_font = "";

void setup() {
  fullScreen();
  //size(1280, 900);
  colorMode(HSB, 360, 100, 100, 100);
  
  RG.init(this); // geomerative
  drop = new SDrop(this);
  main_layer = createGraphics(width, height);
  rec_layer = createGraphics(REC_BUFFER_WIDTH, REC_BUFFER_HEIGHT, JAVA2D);
  videoExport = new VideoExport(this);
  videoExport.setQuality(100, 64);
  videoExport.setFrameRate(30);
  app = new Main(this);
}

/*
 * Main layer (hidden frame buffer) serves as display-size render, without UI.
 * Every frame, this layer is applied to default frame buffer along with UI,
 * so we can hide the UI or vector overlay independently, and we do not have
 * to clear output between frames. We also render vector overlay separately. 
 * 
 * If offscreen capture is allowed, render twice, once into offscreen buffer
 * scaled, with chosen resolution; then in the main layer for the user.
 * This will severely reduce performance and could be optimized in the future.
 */
void draw() {
  app.update();
  
  background(app.v.colors.background);
  if (recording) {
    app.set_frame_buffer(rec_layer);
    app.fbuf.beginDraw();
    if (rec_initializing) {
      app.fbuf.clear();
      
      if (app.v.capture_video) {
        videoExport.setMovieFileName(record_time + "_" + frameCount + ".mp4");
        videoExport.startMovie();
      }
      if (!app.v.transparent_save)
        app.fbuf.background(app.v.colors.background);
      rec_initializing = false;
    }
    app.fbuf.pushMatrix();
    app.fbuf.scale(REC_BUFFER_WIDTH / width);
    app.draw();
    app.fbuf.popMatrix();
    app.fbuf.endDraw();
    app.set_frame_buffer(g);
  }
  
  app.set_frame_buffer(main_layer);
  app.fbuf.beginDraw();
  app.draw();
  app.fbuf.endDraw();
  app.set_frame_buffer(g);
  image(main_layer, 0, 0);
  if (recording && app.v.capture_video)     
    videoExport.saveFrame();
  app.draw_vector_overlay();
}

class Main {
  Visuals v;
  UI ui;
  
  PGraphics vector_layer = createGraphics(width, height); // buffer for vector preview
  PGraphics fbuf; // currently active frame buffer will be stored here
  
  FlowField field;
  ArrayList<Agent> agents = new ArrayList<Agent>();
  RFont font;
  RShape font_shape;
  RGroup font_group;
  RPoint[] font_points;
  
  String text = "Flow";
  boolean show_vectors_temporarily = false, vectors_drawn = false;
  float time = 0;
  
  Main(PApplet applet) {
    v = new Visuals();
    cp5 = new ControlP5(applet);
    ui = new UI(cp5, this);
    
    randomize_rng();
    fbuf = g;
    init_font_pos();
    init();
    ui.setup();
  }
  
  void set_frame_buffer(PGraphics f) {
    fbuf = f;
  }
  
  void redraw() {
    vectors_drawn = false;
    noiseSeed(v.rng_seed);
    randomSeed(v.rng_seed);
    
    fbuf.background(v.colors.background);
    agents.clear();
    populate();
  }
  
  void init() {
    init_font();
    
    fbuf.background(v.colors.background);
    noiseSeed(v.rng_seed);
    randomSeed(v.rng_seed);
    
    field = new FlowField(v.field.resolution, v.field.noise_scale);
    field.init_noise(v.field.animate ? time : 0, v.field.noise_method);
    init_vector_layer();
    redraw();
  }
  
  void init_vector_layer() {
    vector_layer.beginDraw();
    vector_layer.clear();
    field.draw(vector_layer, 
      color(
      360 - hue(v.colors.background), 
      100 - map(saturation(v.colors.background), 0, 100, 100, 30), 
      100 - brightness(v.colors.background))
      );
    vector_layer.endDraw();
  }
  
  void init_font_pos() {
    // set text position intelligently from default visuals
    textSize(v.font.size);
    PFont pf = createFont(v.font.path, v.font.size);
    if (pf == null)
      pf = createFont("Arial", v.font.size);
    textFont(pf);
    if (v.font.pos_x == 0 && v.font.pos_y == 0) {
      v.font.pos_x = v.show_gui ? v.GUI_WIDTH : 0 + width / 2;
      v.font.pos_y += height / 2 + (textAscent() / 5);
    } else {
      v.font.pos_x += v.show_gui ? v.GUI_WIDTH : 0;
      v.font.pos_y += textAscent();
    }
  }
  
  void draw_text() {
    if (v.font.stroke)
      fbuf.stroke(v.colors.font_stroke);
    else
      fbuf.noStroke();
      
    if (v.font.fill)
      fbuf.fill(v.colors.font_fill);
    else
      fbuf.noFill();
    
    fbuf.pushMatrix();
    fbuf.translate(v.font.pos_x, v.font.pos_y);
    font.draw(text);
    fbuf.popMatrix();
  }
  
  void init_font() {
    try {
      font = new RFont(v.font.path, v.font.size, LEFT);
    } catch (NullPointerException e) {
      println("Cannot load font "+v.font.path+"!");
      if (font == null) {
        font = new RFont("Arial", v.font.size, LEFT);
        println("Defaulting to Arial.");
      }
    }
    
    RCommand.setSegmentLength(v.font.rcommand_segment_length);
    RCommand.setSegmentator(RCommand.UNIFORMLENGTH);
    font_shape = font.toShape(text);
      
    font_group = font.toGroup(text);
    font_points = font_group.getPoints();
  }
  
  void spawn_agent(float x, float y) {
    agents.add(
      new Agent(x, y, color(
        random(v.agent.c_min_h, v.agent.c_max_h), 
        random(v.agent.c_min_s, v.agent.c_max_s), 
        random(v.agent.c_min_b, v.agent.c_max_b),  
        random(v.agent.c_min_a, v.agent.c_max_a)), 
        v.agent.size, v.agent.max_force, v.agent.max_speed)
    );
  }
  
  /**
   * Spawn agents on font curves and field
   */
  void populate() {
    if (v.agent.num > 0) {
    // draw agents randomly in field
      for (int i = 0; i < v.agent.num; i++) {
        float x = random(width);
        float y = random(height);
        spawn_agent(x, y);
      }
    }
    
    if (v.agent.spawn_on_start) {
      // draw agents along text curves
      if (font_points != null) {
        for (int i = 0; i < font_points.length - 1; i++) {
          RPoint point = font_points[i];
          float x = point.x + v.font.pos_x;
          float y = point.y + v.font.pos_y;
          spawn_agent(x, y);
        }
      }
    }
  }
  
  /**
   * Spawn agents rather randomly on font curves
   */
  void populate_continuosly() { 
    if (font_points == null)
      return;
      
    if (agents.size() >= v.agent.max)
      return;
      
    int jump = (int)random(50, 100);
    for (int i = 0; i < font_points.length; i = i + jump) {
      RPoint point = font_points[i];
      float x = point.x + v.font.pos_x;
      float y = point.y + v.font.pos_y;
      spawn_agent(x, y);
    }
  }
  
  void remove_offscreen_agents() {
    ArrayList<Agent> rm = new ArrayList<Agent>();
    for (Agent a : agents) {
      float x = a.location.x;
      float y = a.location.y;
      if ((x < -20 || x > width+20)
      || (y < -20 || y > height+20))
        rm.add(a);
    }
    agents.removeAll(rm);
  }
  
  void clear_buffers() {
    clear();
    
    main_layer.beginDraw();
    main_layer.clear();
    main_layer.endDraw();
    
    rec_layer.beginDraw();
    rec_layer.clear();
    rec_layer.endDraw();
  }
  
  void restart_agents() {
    app.agents.clear();
    app.populate();
    if (app.v.agent.spawn_continuosly)
      app.populate_continuosly();
  }
  
  void update() {
    // if user dropped file over window, it will be stored in this global
    if (load_params.length() > 0) {
      cp5.loadProperties(load_params);
      load_params = "";
      app.init();
      app.clear_buffers();
      cp5.get(Textfield.class, "seed").submit();
    }
    if (load_font.length() > 0) {
      v.font.path = load_font;
      load_font = "";
      app.init();
      app.clear_buffers();
    }
    
    // todo: reset some performance heavy settings if fps drops very low
    if (frameRate < 12) {
      v.agent.spawn_continuosly = false;
      cp5.get(Toggle.class, "spawn continuosly").setValue(false);
    }
    remove_offscreen_agents();
    
    time += v.field.anim_speed / 50;
    if (v.field.animate) {
      field.init_noise(time, v.field.noise_method);
      init_vector_layer();
    }  
    
    if (v.agent.spawn_continuosly)
      populate_continuosly();
    
    for (Agent a : agents) {
      a.follow(field);
      a.update();
    }
  }
  
  void draw() {
    if (v.redraw)
      fbuf.background(v.colors.background);
    
    if (v.font.draw)
      draw_text();
    
    for (Agent a : agents)
      a.draw(fbuf);
  }
  
  void draw_vector_overlay() {
    if (!v.field.draw_vectors && !show_vectors_temporarily)
      return;
    g.image(vector_layer, 0, 0);
    vectors_drawn = true;
  }
  
  void randomize_rng() {
    v.rng_seed = (int)random(10000);
  }
}


void keyPressed() {
  if (key == TAB) {
    app.v.show_gui = !app.v.show_gui;
    if (app.v.show_gui) {
      cp5.show();
    } else {
      cp5.hide();
    }
    return;
  }
  
  if (key == '`' || key == ';') {
    if (recording) 
      return;
      
    record_time = get_time();
    cp5.saveProperties(record_time+"_parameters.json");
    app.ui.show_rec(true);
    rec_initializing = true;
    recording = true;
    app.clear_buffers();
    app.restart_agents();
  }
  
  switch(key) {
  case DELETE:
  case BACKSPACE:
    app.text = app.text.substring(0, max(0, app.text.length() - 1));
    app.init_font();
    app.restart_agents();
    app.clear_buffers();
    return;
  case RETURN:
  case ENTER:
    return;
  case ESC:
    exit();
  default:
  }
}

void keyTyped() {
  if (key >= 'A' && key <= 'z' || key == ' ') { 
    app.text += key;
    app.init_font();
    app.restart_agents();
    app.clear_buffers();
    return;
  }
  
  if (!app.v.show_gui) {
    String special = ",.<>?!@#$%^&*()(_-=+[]{}\\/|:";
    if (key >= '0' && key <= '9' || special.indexOf(key) >= 0) { 
      app.text += key;
      app.init_font();
      app.restart_agents();
      app.clear_buffers();
      return;
    }
  }
}

void keyReleased() {
  if (key == '`' || key == ';') {
    if (!recording) 
      return;
    
    if (app.v.capture_video)
      videoExport.endMovie();
    
    if (app.v.capture_offscreen)
      rec_layer.save(record_time+"_hq.png");
    else
      main_layer.save(record_time+".png");
      
    recording = false;
    app.ui.show_rec(false);
  }
}

void mousePressed() {
  if (app.v.show_gui)
    return;
    
  app.v.font.pos_x = mouseX;
  app.v.font.pos_y = mouseY;
  app.clear_buffers();
  app.init_font();
  app.restart_agents();
}

/**
 * Loading program saves, fonts
 * Store file path into global variable, app will handle rest (concurrency issues)
 */
void dropEvent(DropEvent ev) {
  String f = ev.toString();
  if (!ev.isFile()) {
    println("Cannot load "+f);
    return;
  }
  
  if (f.endsWith(".ttf")) {
    load_font = f;
    println("Will load font from "+f);
    return;
  }
  
  if (f.endsWith("parameters.json")) {
    load_params = f;
    println("Will load parameters from "+f);
    return;
  }
  
  println("Unrecognized file type for "+f);
}

String get_time() {
  return year() + nf(month(), 2) + nf(day(), 2) + "-"  + nf(hour(), 2) + nf(minute(), 2) + nf(second(), 2);
}
