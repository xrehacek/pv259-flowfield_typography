public static enum NoiseMethod {
  PERLIN("Perlin: cartesian")
  , PERLINx100("Perlin: cartesian x100")
  , PERLINx500("Perlin: cartesian x500")
  , PERLIN_POLAR("Perlin: polar")
  , KAMPYLE("Kampyle of Eudoxus")
  , ASTROID("Astroid")
  , ASTROID2("Astroid variant")
  , TIME("Time")
  , SWIRL("Swirl")
  ;
  
  private final String val;
  private NoiseMethod(final String val) { this.val = val; }
  @Override
  public String toString() { return val; }
  
  public static String[] toArray() {
    String[] a = new String[NoiseMethod.values().length];
    for (int i = 0; i < NoiseMethod.values().length; i++)
      a[i] = NoiseMethod.values()[i].toString();
    return a;
  }
}

class FlowField {
  static final int 
    IGNORE_DRAW_RESOLUTION_TRESH = 12
    , SIMPLIFIED_DRAW_RESOLUTION_TRESH = 24;
  
  
  final PVector[][] field;
  final float resolution;
  final double noise_scale;
  final int rows, cols;
  
  private float arrowsize = 3;
  
  public FlowField(float resolution, float noise_scale) {
    if (resolution < 1)
        throw new IllegalArgumentException("Constructing field with subpixel resolution is not supported.");
      
    this.resolution = resolution;
    this.noise_scale = noise_scale;
    rows = ceil(height / resolution);
    cols = ceil(width / resolution);
    field = new PVector[cols][rows];
  }
  
  void init_noise(float anim_key, NoiseMethod method) {
    float yoff = 0;
    for (int y = 0; y < rows; y++) {
      float xoff = 0;
      for (int x = 0; x < cols; x++) {
        switch (method) {
          case PERLIN: {
          float n = map(noise(xoff, yoff, anim_key), 0, 1, 0, TWO_PI);
          field[x][y] = PVector.fromAngle(n); //mag*/
          break;
          }
          
          case PERLINx100: {
          float n = 100 * map(noise(xoff/5, yoff/5, anim_key),0,1,-1,1);
          field[x][y] = new PVector(cos(n),sin(n));
          break;
          }
          
          case PERLINx500: {
          float n = 500 * map(noise(xoff/5, yoff/5, anim_key),0,1,-1,1);
          field[x][y] = new PVector(cos(n),sin(n));
          break;
          }
          
          case PERLIN_POLAR: {
          // no conversion to cartesian coords
          float n = map(noise(xoff/5, yoff/5, anim_key), 0, 1, -1, 1);
          field[x][y] = new PVector(n, n);
          break;
          }
          
          case KAMPYLE: {
          float n = 6 * map(noise(xoff, yoff, anim_key), 0, 1, -1, 1);         
          float sec = 1 / sin(n);   
          float xt = sec;
          float yt = tan(n) * sec; 
          field[x][y] = new PVector(xt,yt).normalize(); // normalize?
          break;
          }
          
          case ASTROID: {
          float n = 5 * map(noise(xoff, yoff, anim_key), 0, 1, -1, 1);
          float sinn = sin(n);
          float cosn = cos(n);           
          float xt = sq(sinn)*sinn;
          float yt = sq(cosn)*cosn;
          field[x][y] = new PVector(xt,yt);
          }
          
          case ASTROID2: {
          float n1a = 3*map(noise(xoff, yoff, anim_key),0,1,-1,1);
          float n1b = 3*map(noise(xoff, yoff, anim_key),0,1,-1,1);           
          PVector v1 = Noise.rect_hyperbola(n1a);
          PVector v2 = Noise.astroid(n1b);           
          float n2a = 3*map(noise(v1.x,v1.y, anim_key),0,1,-1,1);
          float n2b = 3*map(noise(v2.x,v2.y, anim_key),0,1,-1,1);           
          field[x][y] = new PVector(cos(n2a),sin(n2b));
          break;
          }
          
          case TIME: {
          float n1a = 3*map(noise(xoff/2, yoff/2, anim_key),0,1,-1,1);
          float n1b = 3*map(noise(xoff/2, yoff/2, anim_key),0,1,-1,1);
          float nn = 6*map(noise(n1a, n1b, anim_key),0,1,-1,1);     
          field[x][y] = Noise.circle(nn); 
          break;
          }
          
          case SWIRL: {
          float n1 = 5*map(noise(xoff/5, yoff/5, anim_key),0,1,-1,1);
          float n2 = 5*map(noise(xoff/5, yoff/5, anim_key),0,1,-1,1); 
          PVector v1 = Noise.vexp(new PVector(n1,n2),1);
          PVector v2 = Noise.swirl(new PVector(n2,n1),1);          
          PVector v3 = PVector.sub(v2,v1);           
          PVector v4 = Noise.waves2(v3,1);
          v4.mult(0.8);
          field[x][y] = new PVector(v4.x,v4.y);
          break;
          }
          

          
          
          
          default:
            throw new RuntimeException("Invalid noise method");
        }
        
        xoff += noise_scale;
      }
      yoff += noise_scale;
    }
  }
  
  /**
   * Draw vectors
   */
  void draw(PGraphics target, color stroke) {
    if (resolution < IGNORE_DRAW_RESOLUTION_TRESH)
      return;
      
    arrowsize = 2 + map(resolution, 1, 100, 0, 10);
    int scale = (int)map(resolution, 0, 100, 0, 32);
        
    target.stroke(stroke);
    target.pushMatrix();
    //target.translate(-(resolution + (width / resolution)), -(resolution + (height / resolution)));
    for (int row = 0; row < rows; row++) {
      for (int col = 0; col < cols; col++) {
        drawVector(target, col, row, scale);
      }
    }
    target.popMatrix();
  }
  
  void drawVector(PGraphics target, int col, int row, float scale) {
    target.pushMatrix();
    // center to each position
    target.translate(col * resolution + (width / resolution / 2), row * resolution + (height / resolution / 2));
    target.rotate(field[col][row].heading());
    float len = field[col][row].mag() * scale;
    
    // arrow
    if (len == 0) {
      target.ellipse(0, 0, 1, 0);
    } else if (len < 2) {
      target.ellipse(0, 0, 2, 0);
    } else {
      target.line(0, 0, len, 0);
      if (resolution > SIMPLIFIED_DRAW_RESOLUTION_TRESH) {
        target.line(len, 0, len-arrowsize, arrowsize/2);
        target.line(len, 0, len-arrowsize, -arrowsize/2);
      }
    }
    target.popMatrix();
  }
  
  PVector lookup(PVector v) {
    int x = int(constrain(v.x / resolution, 0, cols - 1));
    int y = int(constrain(v.y / resolution, 0, rows - 1));
    
    if (field[x][y] == null)
      throw new IllegalStateException("Field vectors are not generated correctly. Found null vector, please check your noise method.");
      
    return field[x][y];
  }
  
  PVector get(int x, int y) {
    return field[constrain(x, 0, cols - 1)][constrain(y, 0, rows - 1)];
  }
}
