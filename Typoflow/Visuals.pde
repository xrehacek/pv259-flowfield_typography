class Visuals {
  Colors colors = new Colors();
  Field field  = new Field();
  Font font    = new Font();
  Agent agent  = new Agent();
  
  final int
    GUI_WIDTH = 500
    , MAX_RESOLUTION = 100
    ;
  
  boolean 
  show_fps = true
  , show_gui = true
  , redraw = false
  , transparent_save = true
  , capture_video = false
  , capture_offscreen = false
  ;
  int
  rng_seed = 0
  ;
  
  class Colors {
    color
      background      = color(0, 0, 0)
      , font_fill     = color(0, 0, 100)
      , font_stroke   = color(0, 0, 100)
      , gui_background = color(230, 10, 50, 100)
      , gui_warn_text = color(360, 35, 100)
      ;
  }
  
  class Field {
    float 
      resolution = 6
      , noise_scale = 0.01
      , anim_speed = 1;
      ;
    boolean
      draw_vectors = false
      , apply_text = false
      , animate = false
      ;
    NoiseMethod noise_method = NoiseMethod.PERLIN;
  }
  
  class Font {
    String path = "fonts/default.ttf";
    int 
      pos_x = 0
      , pos_y = 0
      , size = height / 3
      , rcommand_segment_length = 1
      ;
    boolean
      draw = false, // broken
      fill = true,
      stroke = false;
  }
  
  class Agent {
    boolean
      spawn_on_start = true
      , spawn_continuosly = false
      ;
    float 
      size = 1
      , max_force = 0.2f
      , max_speed = 1f
      ;
    int 
      num = 0
      , num_limit = 20000
      , max = 100000
      , c_min_a = 10
      , c_max_a = 50
      , c_min_h = 0
      , c_max_h = 0
      , c_min_s = 0
      , c_max_s = 0
      , c_min_b = 80
      , c_max_b = 100
      ;
  }
}
