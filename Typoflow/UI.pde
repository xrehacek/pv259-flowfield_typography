import controlP5.*;
  
class UI {
  static final int
    NUMBERBOX_HEIGHT = 22
    , NUMBERBOX_SMALL_WIDTH = 60
    , CHECKBOX_SIDE = 22
    ;
    
  ControlP5 cp5;
  Main app;
  Visuals v;
  
  Textlabel label_draw_vectors_res_too_high;
  
  UI(ControlP5 ui, Main app) {
    this.cp5 = ui;
    this.app = app;
    v = app.v;
  }
  
  void setup() {
    cp5.addTextlabel("label_help")
      .setText("ESC to exit\nTAB toggles UI\nRecord by holding down ;\n\nload save or change font (.ttf)\nby drag and drop")
      .setPosition(width - 150, 20)
      ;
      
    cp5.addTextlabel("label_rec")
      .setText("recording, release to save")
      .setPosition(width - 150, 20)
      .hide()
      ;
    cp5.addFrameRate()
    .setInterval(10)
    .setLabel("fps")
    .setPosition(width - 25, height - 20)
    ;
          
    //
    // GROUP FILE
    //     
    Group g_file = cp5.addGroup("File").setBackgroundColor(v.colors.gui_background).setBackgroundHeight(120);
    cp5.addToggle("capture video")
      .moveTo(g_file)
      .setPosition(10, 10)
      .setSize(CHECKBOX_SIDE, CHECKBOX_SIDE)
      .setValue(v.capture_video)
      .plugTo(this, "set_capture_video")
      ;
    cp5.addToggle("capture highres images (affects speed)")
      .moveTo(g_file)
      .setPosition(90, 10)
      .setSize(CHECKBOX_SIDE, CHECKBOX_SIDE)
      .setValue(v.capture_offscreen)
      .plugTo(this, "set_capture_offscreen")
      ;
    
    cp5.addTextfield("seed")
    .moveTo(g_file)
    .setValue(v.rng_seed)
    .setText(Integer.toString(v.rng_seed))
     .setPosition(10,60)
     .setSize(190, CHECKBOX_SIDE)
     .setAutoClear(false)
     .plugTo(this, "set_rng_seed")
     ;
    cp5.addBang("set_new_seed")
    .moveTo(g_file)
    .plugTo(this, "set_new_seed")
     .setLabel("new")
     .setPosition(210,60)
     .setSize(50, NUMBERBOX_HEIGHT)
     .getCaptionLabel().align(ControlP5.CENTER, ControlP5.CENTER)
     ;
    
    //
    // GROUP FLOWFIELD
    //
    Group g_flowfield = cp5.addGroup("Flowfield").setBackgroundColor(v.colors.gui_background).setBackgroundHeight(400);
    
    cp5.addSlider("resolution")
      .setValue(v.field.resolution)
      .plugTo(this, "set_resolution")
      .moveTo(g_flowfield)
      .setPosition(10, 10)
      .setRange(v.MAX_RESOLUTION,1)
      .setSize(250,NUMBERBOX_HEIGHT)
      .addCallback(new CallbackListener() {
      public void controlEvent(CallbackEvent ev) {
        if (ev.getAction()==ControlP5.ACTION_ENTER) {
          if (!v.field.draw_vectors
          && v.field.resolution >= FlowField.IGNORE_DRAW_RESOLUTION_TRESH)
            app.show_vectors_temporarily = true;
        }
        if (ev.getAction()==ControlP5.ACTION_LEAVE) {
          if (app.show_vectors_temporarily == true) {
            app.show_vectors_temporarily = false;
          }
        }
      }
    });
    cp5.addTextlabel("label_resolution")
      .moveTo(g_flowfield)
      .setText("determines granularity of flowfield:\n1 means perpixel, 50 means vector every 1/50 of screen width")
      .setPosition(10, 35)
      ;

      // todo: change to buttons x2 x10 x100
      Numberbox nbox_noise_scale = cp5.addNumberbox("noise scale")
      .moveTo(g_flowfield)
      .setPosition(10, 90)
      .setSize(250,NUMBERBOX_HEIGHT)
      .setDecimalPrecision(10)
      .setMultiplier(0.001) // set the sensitifity
      .setScrollSensitivity(0.001)
      .setDirection(Controller.VERTICAL)
      .setValue((float) v.field.noise_scale)
      .plugTo(this, "set_noise_scale")
      ;
      makeEditable(nbox_noise_scale); 
      
      // noise method controller follows (for correct draw order)
      
      cp5.addToggle("animate")
      .moveTo(g_flowfield)
      .setPosition(10, 135)
      .setSize(CHECKBOX_SIDE, CHECKBOX_SIDE)
      .setValue(v.field.animate)
      .plugTo(this, "set_animate")
      ;
      cp5.addSlider("speed")
      .setValue(v.field.anim_speed)
      .plugTo(this, "set_anim_speed")
      .moveTo(g_flowfield)
      .setPosition(40, 135)
      .setRange(0,1)
      .setSize(220,NUMBERBOX_HEIGHT);
      
      cp5.addToggle("show vectors")
      .moveTo(g_flowfield)
      .setPosition(10, 180)
      .setSize(CHECKBOX_SIDE, CHECKBOX_SIDE)
      .setValue(v.field.draw_vectors)
      .plugTo(this, "set_draw_vectors")
      ;
      label_draw_vectors_res_too_high = cp5.addTextlabel("label_resolution_too_high")
      .moveTo(g_flowfield)
      .setText("ignored until resolution is reduced")
      .setPosition(35, 180)
      .setColorValue(int(v.colors.gui_warn_text))
      .hide()
      ;
      
      cp5.addToggle("redraw")
      .moveTo(g_flowfield)
      .setPosition(238, 180)
      .setSize(CHECKBOX_SIDE, CHECKBOX_SIDE)
      .setValue(v.redraw)
      .plugTo(this, "set_redraw")
      ;
      
      cp5.addColorWheel("c_background", 0, 150, 150)
      .setLabel("Background color")
      .setRGB(v.colors.background)
      .moveTo(g_flowfield)
      .setPosition(10, 225)
      .addCallback(new CallbackListener() {
        public void controlEvent(CallbackEvent ev) {
          if (ev.getAction() == ControlP5.ACTION_BROADCAST) {
            v.colors.background = color(cp5.get(ColorWheel.class, "c_background").getRGB());
            app.init_vector_layer();
          }
        }
      });
      
    // must be last so it renders over previous controllers
    cp5.addScrollableList("noise method")
     .moveTo(g_flowfield)
     .setSize(250, 100)
     .setPosition(10, 65)
     .setBarHeight(20)
     .setItemHeight(20)
     .addItems(NoiseMethod.toArray())
     .setType(ScrollableList.DROPDOWN)
     .setOpen(false)
     .setValue(v.field.noise_method.ordinal())
     .plugTo(this, "set_noise_method")
     ;
    
    //
    // GROUP TEXT
    //
    Group g_text = cp5.addGroup("Text").setBackgroundColor(v.colors.gui_background).setBackgroundHeight(105);
    
    cp5.addTextlabel("label_text")
      .moveTo(g_text)
      .setText("Type into canvas. New lines are not supported.\n\nWhen you hide UI, special characters and numbers are allowed.")
      .setPosition(10, 10)
      ;

      cp5.addTextlabel("label_pos")
      .moveTo(g_text)
      .setText("position")
      .setPosition(10, 45)
      ;
      Numberbox nbox_pos_x = cp5.addNumberbox("x")
      .moveTo(g_text)
      .setPosition(10, 60)
      .setSize(NUMBERBOX_SMALL_WIDTH,NUMBERBOX_HEIGHT)
      .setDirection(Controller.VERTICAL)
      .setMultiplier(1)
      .setScrollSensitivity(1)
      .setValue(v.font.pos_x)
      .plugTo(this, "set_text_pos_x")
      ;
      makeEditable(nbox_pos_x);
      
      makeEditable(cp5.addNumberbox("y")
      .moveTo(g_text)
      .setPosition(10 + NUMBERBOX_SMALL_WIDTH + 5, 60)
      .setSize(NUMBERBOX_SMALL_WIDTH,NUMBERBOX_HEIGHT)
      .setDirection(Controller.VERTICAL)
      .setMultiplier(1)
      .setScrollSensitivity(1)
      .setValue(v.font.pos_y)
      .plugTo(this, "set_text_pos_y")
      );
      
      makeEditable(cp5.addNumberbox("font size")
      .moveTo(g_text)
      .setPosition(10 + NUMBERBOX_SMALL_WIDTH*2 + 40, 60)
      .setSize(NUMBERBOX_SMALL_WIDTH,NUMBERBOX_HEIGHT)
      .setDirection(Controller.VERTICAL)
      .setMultiplier(1)
      .setScrollSensitivity(1)
      .setValue(v.font.size)
      .plugTo(this, "set_font_size")
      );
      
    //
    // GROUP AGENT
    //
    Group g_agent = cp5.addGroup("Agent").setBackgroundColor(v.colors.gui_background).setBackgroundHeight(450);
    
    cp5.addTextlabel("label_agent1")
      .moveTo(g_agent)
      .setText("randomly located on canvas")
      .setPosition(10, 10)
      ;
      
      cp5.addSlider("number")
      .moveTo(g_agent)
      .setPosition(10, 25)
      .setRange(0, v.agent.num_limit)
      .setSize(250, NUMBERBOX_HEIGHT)
      .setValue(v.agent.num)
      .plugTo(this, "set_agent_num")
      ;
   
      cp5.addScrollableList("follow behaviour")
       .moveTo(g_agent)
       .setSize(250, 350)
       .setPosition(10, 50)
       .setBarHeight(20)
       .setItemHeight(20)
       //.addItems()
       .setType(ScrollableList.DROPDOWN) // supported DROPDOWN and LIST
       .setOpen(false)
       //.setValue(0)
       .plugTo(this, "set_agent_behaviour")
       ;
       
      cp5.addSlider("max. speed")
      .moveTo(g_agent)
      .setPosition(10, 80)
      .setRange(1, 10)
      .setSize(250, NUMBERBOX_HEIGHT)
      .setValue(v.agent.max_speed)
      .plugTo(this, "set_agent_max_speed")
      ;
      
      cp5.addSlider("max. force")
      .moveTo(g_agent)
      .setPosition(10, 110)
      .setRange(0, 10)
      .setSize(250, NUMBERBOX_HEIGHT)
      .setValue(v.agent.max_force)
      .plugTo(this, "set_agent_max_force")
      ;
      
    cp5.addColorWheel("c_agent_fill", 0, 150, 150)
      .setLabel("Fill color")
      .setHSL(
        (v.agent.c_min_h + v.agent.c_max_h) / 2,
        (v.agent.c_min_s + v.agent.c_max_s) / 2,
        (v.agent.c_min_b + v.agent.c_max_b) / 2)
      .moveTo(g_agent)
      .setPosition(10, 140)
      .addCallback(new CallbackListener() {
        public void controlEvent(CallbackEvent ev) {
          if (ev.getAction()==ControlP5.ACTION_BROADCAST) {
            int c = cp5.get(ColorWheel.class, "c_agent_fill").getRGB();
            v.agent.c_min_h = (int)hue(c);
            v.agent.c_max_h = (int)hue(c);
            v.agent.c_min_s = (int)saturation(c);
            v.agent.c_max_s = (int)saturation(c);
            v.agent.c_min_b = (int)brightness(c);
            v.agent.c_max_b = (int)brightness(c);
            cp5.get(Range.class, "hue range").setRangeValues(v.agent.c_min_h, v.agent.c_max_h);
            cp5.get(Range.class, "saturation range").setRangeValues(v.agent.c_min_s, v.agent.c_max_s);
            cp5.get(Range.class, "brightness range").setRangeValues(v.agent.c_min_b, v.agent.c_max_b);
          }
        }
      });
      
    cp5.addToggle("spawn all on start")
      .moveTo(g_agent)
      .setPosition(180, 140)
      .setSize(CHECKBOX_SIDE, CHECKBOX_SIDE)
      .setValue(v.agent.spawn_on_start)
      .plugTo(this, "set_spawn_on_start")
      ;
     cp5.addToggle("spawn continuosly")
      .moveTo(g_agent)
      .setPosition(180, 180)
      .setSize(CHECKBOX_SIDE, CHECKBOX_SIDE)
      .setValue(v.agent.spawn_continuosly)
      .plugTo(this, "set_spawn_continuosly")
      ;
     cp5.addBang("clear")
     .moveTo(g_agent)
     .setPosition(180, 220)
     .setSize(50, CHECKBOX_SIDE)
     .setTriggerEvent(Bang.RELEASE)
     .plugTo(this, "clear_agents")
     ; 
      
    cp5.addRange("opacity range")
    .moveTo(g_agent)
     // disable broadcasting since setRange and setRangeValues will trigger an event
     .setBroadcast(false) 
     .setPosition(10,310)
     .setSize(220,22)
     .setHandleSize(20)
     .setRange(1,100)
     .setRangeValues(v.agent.c_min_a, v.agent.c_max_a)
     // after the initialization we turn broadcast back on again
     .setBroadcast(true)
     .setColorForeground(color(255,40))
     .setColorBackground(color(255,40))  
     .addListener(new ControlListener() {
        public void controlEvent(ControlEvent ev) {
          v.agent.c_min_a = int(ev.getController().getArrayValue(0));
          v.agent.c_max_a = int(ev.getController().getArrayValue(1));
        }
      });
      
     cp5.addRange("hue range")
    .moveTo(g_agent)
     // disable broadcasting since setRange and setRangeValues will trigger an event
     .setBroadcast(false) 
     .setPosition(10,335)
     .setSize(220,22)
     .setHandleSize(20)
     .setRange(0,360)
     .setRangeValues(v.agent.c_min_h, v.agent.c_max_h)
     // after the initialization we turn broadcast back on again
     .setBroadcast(true)
     .setColorForeground(color(255,40))
     .setColorBackground(color(255,40))  
     .addListener(new ControlListener() {
        public void controlEvent(ControlEvent ev) {
          v.agent.c_min_h = int(ev.getController().getArrayValue(0));
          v.agent.c_max_h = int(ev.getController().getArrayValue(1));
        }
      });
      
    cp5.addRange("saturation range")
    .moveTo(g_agent)
     // disable broadcasting since setRange and setRangeValues will trigger an event
     .setBroadcast(false) 
     .setPosition(10,360)
     .setSize(220,22)
     .setHandleSize(20)
     .setRange(0,100)
     .setRangeValues(v.agent.c_min_s, v.agent.c_max_s)
     // after the initialization we turn broadcast back on again
     .setBroadcast(true)
     .setColorForeground(color(255,40))
     .setColorBackground(color(255,40))  
     .addListener(new ControlListener() {
        public void controlEvent(ControlEvent ev) {
          v.agent.c_min_s = int(ev.getController().getArrayValue(0));
          v.agent.c_max_s = int(ev.getController().getArrayValue(1));
        }
      });
      
    cp5.addRange("brightness range")
    .moveTo(g_agent)
     // disable broadcasting since setRange and setRangeValues will trigger an event
     .setBroadcast(false) 
     .setPosition(10,385)
     .setSize(220,22)
     .setHandleSize(20)
     .setRange(0,100)
     .setRangeValues(v.agent.c_min_b, v.agent.c_max_b)
     // after the initialization we turn broadcast back on again
     .setBroadcast(true)
     .setColorForeground(color(255,40))
     .setColorBackground(color(255,40))  
     .addListener(new ControlListener() {
        public void controlEvent(ControlEvent ev) {
          v.agent.c_min_b = int(ev.getController().getArrayValue(0));
          v.agent.c_max_b = int(ev.getController().getArrayValue(1));
        }
      });
      
    //
    // MENU ACCORDION
    //
    Accordion accordion = cp5.addAccordion("acc")
     .setPosition(20,20)
     .setWidth(320)
     .addItem(g_file)
     .addItem(g_flowfield)
     .addItem(g_text)
     .addItem(g_agent)
     ;
     
     // allow multiple groups to be opened at the same time
     accordion.setCollapseMode(Accordion.MULTI);
     accordion.open(1, 2, 3);
  }
  
  
  
  
  // *************************************************************
  // ACTIONS
  // *************************************************************

  //
  // FIELD
  //
  void set_resolution(int n) {
    if (v.field.resolution == n)
      return;
    v.field.resolution = n;
    app.init();
    app.clear_buffers();
    app.restart_agents();
    warn_high_resolution();
  }
  
  void set_animate(boolean b) {
    v.field.animate = b;
  }
  
  void set_anim_speed(float n) {
    v.field.anim_speed = n;
  }
  
  void set_noise_scale(float n) {
    if (v.field.noise_scale == n)
      return;
    v.field.noise_scale = n;
    app.init();
    app.clear_buffers();
    app.restart_agents();
  }
  
  void set_draw_vectors(boolean b) {
    v.field.draw_vectors = b;
    
    if (!b && label_draw_vectors_res_too_high != null) {
        label_draw_vectors_res_too_high.hide();
    }
    warn_high_resolution();
  }
  
  void warn_high_resolution() {
    if (label_draw_vectors_res_too_high == null)
      return;
      
    if (v.field.draw_vectors && v.field.resolution < FlowField.IGNORE_DRAW_RESOLUTION_TRESH)
      label_draw_vectors_res_too_high.show();
    else 
      label_draw_vectors_res_too_high.hide();
  }

  void set_noise_method(int n) {
    v.field.noise_method = NoiseMethod.values()[n];
    app.clear_buffers();
    app.init();
  }
  
  void set_redraw(boolean b) {
    v.redraw = b;
  }
  
  //
  // Text
  //
  void set_text_pos_x(int n) {
    if (n == v.font.pos_x)
      return;
    v.font.pos_x = n;
    app.init();
  }
  
  void set_text_pos_y(int n) {
    if (n == v.font.pos_y)
      return;
    v.font.pos_y = n;
    app.init();
  }
  
  void set_font_size(int n) {
    if (v.font.size == n)
      return;
    v.font.size = n;
    app.init();
  }
  
  //
  // AGENT
  //
  void set_agent_num(int n) {
    if (n == v.agent.num)
      return;
    v.agent.num = constrain(n, 0, v.agent.num_limit);
    app.init();
  }
  
  void set_agent_max_speed(float n) {
    if (n == v.agent.max_speed)
      return;
    v.agent.max_speed = n;
    
    for (Agent a : app.agents)
      a.maxspeed = n;
  }
  
  void set_agent_max_force(float n) {
    if (n == v.agent.max_force)
      return;
    v.agent.max_force = n;
    
    for (Agent a : app.agents)
      a.maxforce = n;
  }
  
  void set_spawn_on_start(boolean b) {
    v.agent.spawn_on_start = b;
    if (b)
      app.init();
  }
  
  void set_spawn_continuosly(boolean b) {
    v.agent.spawn_continuosly = b;
  }
  
  void clear_agents() {
    app.clear_buffers();
    app.restart_agents();
  }
  
  //
  // RNG
  //
  void set_new_seed() {
    app.randomize_rng();
    cp5.get(Textfield.class, "seed").setText(Integer.toString(v.rng_seed));
    app.init();
    main_layer.clear();
    println("Set rng seed to "+v.rng_seed);
  }
  
  void set_rng_seed(String s) {
    int n = 0;
    try {
      n = Integer.parseInt(s);
    } catch (NumberFormatException e) {
      set_new_seed();
    }
    
    if (v.rng_seed == n)
      return;
    v.rng_seed = n;
    app.init();
    app.clear_buffers();
    cp5.get(Textfield.class, "seed").setText(Integer.toString(v.rng_seed));
  }
  
  void set_capture_video(boolean b) {
    v.capture_video = b;
  }
  
  void set_capture_offscreen(boolean b) {
    v.capture_offscreen = b;
  }
  
  void show_rec(boolean b) {
    if (b) {
      cp5.get("label_rec").show();
      cp5.get("label_help").hide();
    } else {
      cp5.get("label_rec").hide();
      cp5.get("label_help").show();
    }
  }
    
}
