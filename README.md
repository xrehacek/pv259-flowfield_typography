# Generovanie typografie pomocou flowfields

Nástroj experimentuje s časticovým systémom v 2D vektorovom poli, na ktoré je namapovaná plynulá šumová funkcia (gradient noise). Pole riadi pohyb častíc, ktoré zanechávajú stopy, vytvárajúc písmo.
Generovanie závisí na veľkom množstve premenných, ktoré síce mätú užívateľa, no umožňujú mu nájsť unikátny vizuálny výstup.
Počas záznamu môže užívateľ naďalej manipulovať s parametrami, nadväzujúc na princíp VJingu. 
Tok častíc umožňuje aj výstup vo forme videa, dobre využiteľného v nových médiách.
Výstup je po uložení parametrov reprodukovateľný.

Nástroj bol vytvorený ako záverečná úloha predmetu PV259 na voľnú tému.

![Ukážka z výstavy](exhibition/Typoflow.mp4)

---

## Spustenie

- prostredie Processing https://processing.org/download
- Java 8 (pre Processing) https://java.com/download

__(voliteľné)__ Na nahrávanie je potrebný Ffmpeg na export videa:
- GNU/Linux: použi package manager
- Windows: statické binárky k dispozícii na http://ffmpeg.zeranoe.com/builds
- Mac: http://evermeet.cx/ffmpeg

Program sa pri prvom nahrávaní spýta na jeho umiestnenie.

### Windows-x64

Pripravený build je k dispozícii v [/build-windows64](/build-windows64)

---

## Kompilácia

Vyžadované knihovne pre Processing:
Geomerative, ControlP5, Drop, VideoExport

---

## Ukážky výstupov

![Výstupy – plagát](exhibition/examples.jpg)
